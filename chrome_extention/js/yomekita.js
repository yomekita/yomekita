/*jslint browser:true, devel:true */
/*global jQuery, EventSource */

/*
 * 設定
 */
var serverSentEventsSource = 'http://localhost/beacon/status/', // Server-Sent EventsのURL
    nikkeiArticleURL = 'http://www.nikkei.com/article/', // 日経の記事のURL
    nikkeiTopArticlesURL = 'http://dac2.snnm.net:80/api/article?uid=webkan_top', // 日経のトップニュースのURL
    nikkeiApplicationID = 'bdf98e42d41e43b18e076c878b2f9c03'; // 日経アプリケーションID

/*
 * 日経のトップニュースに移動
 */
function moveNikkeiTopArticle() {
    'use strict';

    // 日経のトップニュースのkiji_idを取得
    jQuery.ajax({
        url: nikkeiTopArticlesURL,
        type: 'get',
        headers: {
            'X-Nikkei-Application-Id': nikkeiApplicationID
        },
        success: function (data) {
            console.log('success: nikkei top articles have gotten.');
            console.log("nikkei top article's kiji_id: " + data.articles[0].kiji_id);

            // 日経のトップニュースのkiji_idが取得できたら、そこに遷移する
            location.href = nikkeiArticleURL + data.articles[0].kiji_id;
        },
        error: function () {
            console.log('error: nikkei top articles have not gotten.');
        }
    });
}


/*
 * 「嫁来た！」ボタンの作成
 */
function createYomekitaButton(yomekita) {
    'use strict';
    var yomekitaButton = jQuery('<button/>');
    yomekitaButton.text("嫁来た！");
    yomekitaButton.attr("id", "yomekitaButton");
    yomekitaButton.css({
        "position": "absolute",
        "top": "0px",
        "right": "0px",
        "width": "128px",
        "height": "128px",
        "zIndex": 0x7FFFFFFF,
        "backgroundColor": "yellow",
        "borderStyle": "ridge",
        "borderColor": "black",
        "borderWidth": "2px",
        "fontSize": "24px",
        "fontWeight": "bold"
    });
    yomekitaButton.on("click", yomekita);
    jQuery("body").append(yomekitaButton);
}


/*
 * 嫁が来た！ときに実行する内容
 */
function yomekita() {
    'use strict';
    moveNikkeiTopArticle();
}

/*
 * Server-Sent Eventsのロード
 */
function serverSentEventsLoad(serverSentEventsSource) {
    'use strict';

    // Server-Sent Eventsの取得
    var evtSource = new EventSource(serverSentEventsSource);

    // 取得したdataのstatusがtrueになったら、yomekita()を起動
    evtSource.onmessage = function (e) {
        var json = JSON.parse(e.data);
        if (json.status === "true") {
            yomekita();
        }
    };

    // 嫁来た！ボタンの生成
    createYomekitaButton(yomekita);
}

/*
 * 起動プログラム
 */
jQuery(document).ready(function () {
    'use strict';
    console.log('document ready.');
    serverSentEventsLoad(serverSentEventsSource);
});
