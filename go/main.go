package main

import (
	"fmt"
	"io/ioutil"
	"net"
	"net/http"
	"net/http/fcgi"
	"regexp"
	"strconv"
	"strings"
	"time"
)

var (
	beaconCommand = regexp.MustCompile(`^/beacon/status/?.*`)
	smileCommand  = regexp.MustCompile(`^/face/smile/?.*`)
)

type FastCGIServer struct {
	status      bool
	smile_point int
}

func (f *FastCGIServer) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	fmt.Println("ServeHTTP")

	url := r.URL.Path
	fmt.Println(url)
	switch {
	case beaconCommand.MatchString(url):
		fmt.Println("beaconCommand")
		f.beaconHandler(w, r)
	case smileCommand.MatchString(url):
		fmt.Println("smileCommand")
		f.smileHandler(w, r)
	}
}

func (f *FastCGIServer) beaconHandler(w http.ResponseWriter, r *http.Request) {
	flusher, ok := w.(http.Flusher)
	if !ok {
		http.Error(w, "Streaming unsupported!", http.StatusInternalServerError)
		return
	}

	switch r.Method {
	case "GET":
		w.Header().Set("Content-Type", "text/event-stream")
		w.Header().Set("Cache-Control", "no-cache")
		w.Header().Set("Connection", "keep-alive")
		w.Header().Set("Transfer-Encoding", "chunked")
		w.Header().Set("Access-Control-Allow-Origin", "*")

		for n := 0; n < 2; n++ {
			time.Sleep(2 * time.Second)
			fmt.Fprintf(w, "data: %t\n\n", f.status)
			flusher.Flush()
			fmt.Println("Flushed!")
		}

		break
	case "POST":
		recvStatus := strings.Split(r.URL.Path[1:], "/")[2]
		status, err := strconv.ParseBool(recvStatus)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		f.status = status
		if status {
			err := alermLight()
			if err != nil {
				fmt.Println(err)
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}
		}
		w.WriteHeader(http.StatusOK)
	}
}

func (f *FastCGIServer) smileHandler(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case "GET":
		fmt.Fprintf(w, "data: %d\n\n", f.smile_point)
	case "POST":
		urls := strings.Split(r.URL.Path[1:], "/")
		if len(urls) < 3 {
			http.Error(w, "not support parameter", http.StatusInternalServerError)
			return
		}
		smile_point, err := strconv.Atoi(urls[2])
		if err != nil {
			fmt.Println(err)
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		f.smile_point = smile_point
		w.WriteHeader(http.StatusOK)
	}
}

func alermLight() error {
	for _, command := range []string{`{"on":true, "hue":25500}`, `{"on":false}`, `{"on":true, "hue":65535}`, `{"on":false}`, `{"on":true, "hue":46920}`, `{"on":false}`} {
		fmt.Println(command)
		err := doPut("http://192.168.0.2/api/newdeveloper/lights/1/state", command)
		if err != nil {
			return err
		}
		time.Sleep(2 * time.Second)
	}
	return nil
}

func doPut(url, value string) error {
	client := &http.Client{}
	request, err := http.NewRequest("PUT", url, strings.NewReader(value))
	request.ContentLength = int64(len(value))
	response, err := client.Do(request)
	if err != nil {
		return err
	}

	defer response.Body.Close()
	contents, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return err
	}
	if strings.Index(string(contents), "error") != -1 {
		return fmt.Errorf("%s", contents)
	}
	return nil
}
func main() {
	l, _ := net.Listen("tcp", ":9000")
	srv := new(FastCGIServer)
	fcgi.Serve(l, srv)
}
