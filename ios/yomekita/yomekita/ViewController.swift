//
//  ViewController.swift
//  yomekita
//
//  Created by 135yshr on 2014/07/09.
//  Copyright (c) 2014 135yshr. All rights reserved.
//

import UIKit
import CoreLocation

class ViewController: UIViewController, CLLocationManagerDelegate {
    
    var locationMngr: CLLocationManager!
    var beaconRegion: CLBeaconRegion!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.locationMngr = CLLocationManager()
        self.locationMngr.delegate = self
        
        self.beaconRegion = CLBeaconRegion(proximityUUID: NSUUID(), identifier: "Beacon Region")

        // Calling the delegate when it is running in the background.
        self.beaconRegion.notifyEntryStateOnDisplay = false
        
        // Notify when entering the range of the radio waves.
        self.beaconRegion.notifyOnEntry = true
        
        // Notify when it was out of range the radio waves.
        self.beaconRegion.notifyOnExit = true
        
        // start of monitoring.
        self.locationMngr.startMonitoringForRegion(self.beaconRegion)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // Started Monitoring Events
    func locationManager(manager: CLLocationManager!, didStartMonitoringForRegion region: CLRegion!) {
        // already beacon range request.
        locationMngr.requestStateForRegion(self.beaconRegion)
    }
    
    // included of beacon range.
    func locationManager(manager: CLLocationManager!, didEnterRegion region: CLRegion!) {
        NSLog("didEnterRegion")
        locationMngr.startRangingBeaconsInRegion(self.beaconRegion)
    }
    
    // exit of beacon range.
    func locationManager(manager: CLLocationManager!, didExitRegion region: CLRegion!) {
        NSLog("didExitRegion")
    }
    
    //
    func locationManager(manager: CLLocationManager!, didDetermineState state: CLRegionState, forRegion region: CLRegion!) {
        // CLRegionStateUnknown 0
        // CLRegionStateInside 1
        // CLRegionStateOutside 2
        NSLog("locationManager: didDetermineState \(state)")
        switch(state) {
        case CLRegionState.Inside:
            NSLog("Inside");
            locationMngr.startRangingBeaconsInRegion(self.beaconRegion)
            break
        case CLRegionState.Outside:
            NSLog("Outside");
            break
        case CLRegionState.Unknown:
            NSLog("Unknown");
            break
        default:
            NSLog("default");
        }
    }
    
    //
    func locationManager(manager: CLLocationManager!, didRangeBeacons beacons: [AnyObject]!, inRegion region: CLBeaconRegion)  {
        NSLog("locationManager: didRangeBeacons: number of beacons : \(beacons.count)");
        NSLog("--------------------------");
        if(beacons.count == 0) {
            NSLog("out of range");
            return
        }
        
        for beacon in beacons {
            let beaconUUID = beacon.proximityUUID
            let minorID = beacon.minor
            let majorID = beacon.major
            let ssid = beacon.rssi
            
            NSThread.sleepForTimeInterval(1)
            
            
            var q_global = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)
            var q_main: dispatch_queue_t  = dispatch_get_main_queue()
            
            let req = NSMutableURLRequest()
            req.HTTPMethod = "POST"
            req.cachePolicy = NSURLRequestCachePolicy.ReloadIgnoringLocalCacheData
            req.timeoutInterval = 20
            req.HTTPShouldHandleCookies = false
            req.URL = NSURL.URLWithString("http://192.168.8.107/beacon/status")
            
            var res: AutoreleasingUnsafePointer<NSURLResponse?> = nil
            var error: NSErrorPointer!
            NSURLConnection.sendSynchronousRequest(req, returningResponse: res, error: error!)
            if error.getLogicValue() {
                NSLog("\(error)")
            }
            NSLog("sended")
        }
    }
}

