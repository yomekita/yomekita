iBeacon受信PHP
--------------

API仕様
=======
1. 状態の取得 GET /beacon/  
  以下のいずれかのようにJSON形式で状態が返る  
  {"beacon":{"status":"true"}}  
  {"beacon":{"status":"false"}}

2. 状態の設定 POST /beacon/  
  POSTするデータはstatus=true、あるいはstatus=false  
  POSTに対しては、以下のいずれかのようにJSON形式で状態が返る  
  {"beacon":{"status":"true"}}  
  {"beacon":{"status":"false"}}

データファイル
=============
* config.php: cocoroboをコントロールするURLやiBeaconのステータス・ファイルを指定
* secret/status.txt: iBeaconのステータス・ファイル(config.phpで変更可能)。ステータスをtrue/falseで保存
