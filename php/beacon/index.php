<?php
require_once('config.php');
require_once('lib/beacon_lib.php');

$method = $_SERVER["REQUEST_METHOD"];

if ($method == "GET") {
  // HTTPメソッドがGETの場合

  // ステータスを読み出し
  $status = getStatusFromFile($beacon_status_file);

  // ステータスをJSONでレンダリング
  renderJsonStatus($status);

} else if ($method == "POST") {
  // HTTPメソッドがPOSTの場合

  $posted_status = $_POST['status'];
  if ($posted_status == 'true' || $posted_status == "false") {
    // 以前のステータスを読み出し
    $status = getStatusFromFile($beacon_status_file);

    // ステータスが変更された場合だけ書き込む
    if ($posted_status != $status) {
      // ステータスを書き込み
      setStatusToFile($beacon_status_file, $posted_status);

      // ステータスを読み出し
      $status = getStatusFromFile($beacon_status_file);

      // COCOROBOに発話させる
      if ($status == 'true') {
        //var_dump($cocorobo_access_url);
        file_get_contents($cocorobo_access_url);
      }
    }

    // ステータスをJSONでレンダリング
    renderJsonStatus($status);
  }
}
