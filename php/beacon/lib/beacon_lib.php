<?php

// ファイルにステータスを設定
function setStatusToFile($beacon_status_file, $string) {
  file_put_contents($beacon_status_file, $string);
}

// ファイルからステータスを取得
function getStatusFromFile($beacon_status_file) {
  $string = file_get_contents($beacon_status_file);
  return $string;
}

// ステータスをJSONで表示
function renderJsonStatus($status) {
  $data = array(
    'beacon' => array(
      'status' => $status
    )
  );

  header('Content-Type: application/json');
  echo json_encode($data);
}
