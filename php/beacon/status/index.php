<?php
require_once('../config.php');
require_once('../lib/beacon_lib.php');

header("Content-Type: text/event-stream\n\n");
header("Access-Control-Allow-Origin: *\n\n");

while (1) {
  $result = getStatusFromFile('../' . $beacon_status_file);
  if ($result != $recent_result) {
    echo 'data: { "status":"' . $result . '" }'. "\n\n";
  }
  $recent_result = $result;
  ob_flush();
  flush();
  sleep(1);
}
