<?php
// COCOROBO認証のURL
$cocorobo_auth_url = "https://trial.cloudlabs.sharp.co.jp/cocorobo-api/ceAuth";

// COCOROBO音声発話のURL
$cocorobo_speech_url = "https://trial.cloudlabs.sharp.co.jp/cocorobo-api/speech";

// COCOROBO認証データのファイル
$cocorobo_auth_data_file = "secret/auth.txt";

// COCOROBO音声発話データのファイル
$cocorobo_speech_data_file = "speech_text/speech.txt";
