<?php
require_once('config.php');
require_once('lib/cocorobo_lib.php');

// 認証データの読み出し
$cocorobo_auth_data = cocoroboReadAuthDataFromFile($cocorobo_auth_data_file);
//$cocorobo_auth_data = cocoroboReadAuthDataFromEnv();
//var_dump($cocorobo_auth_data);

// 発話データの読み出し
//$cocorobo_message = "教育事業大手のベネッセコーポレーション（岡山市）は社員が休日を分散取得できる取り組みを始めた。休祝日に出社する代わりに実験的に平日に休めるようにし、すでにゴールデンウイーク（ＧＷ";

$cocorobo_speech_data = cocoroboReadSpeechData($cocorobo_speech_data_file);
$cocorobo_speech_text = cocoroboSelectSpeech($cocorobo_speech_data);
//var_dump($cocorobo_speech_text);

// COCOROBOのサーバーへの接続
$response = cocoroboAuthenticate($cocorobo_auth_url, $cocorobo_auth_data);
//var_dump($response);

$response = cocoroboSpeak($cocorobo_speech_url, $cocorobo_auth_data, $cocorobo_speech_text);
//var_dump($response);
