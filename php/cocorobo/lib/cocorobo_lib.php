<?php

// ファイルに保存された認証データの読み出し
function cocoroboReadAuthDataFromFile($auth_file) {
  $auth_data = array();

  // 認証ファイルを開く
  $string = file_get_contents($auth_file);

  // 1行ごとに分解
  $lines = explode("\r\n", $string);

  foreach($lines as $line) {
    // 「:」で分解
    list($property, $value) = explode(":", $line);
    // account、password、productId、userAgentのときだけ、値を設定する
    if ($property == "account") {
      $auth_data['account'] = $value;
    } else if ($property == "password") {
      $auth_data['password'] = $value;
    } else if ($property == "productId") {
      $auth_data['productId'] = (int)$value;
    } else if ($property == "userAgent") {
      $auth_data['userAgent'] = $value;
    }
  }

  return $auth_data;
}

// 環境変数に保存された認証データの読み出し
function cocoroboReadAuthDataFromEnv() {
  $auth_data = array();

  $account = getenv('cocoroboAccount');
  if (isset($account)) {
    $auth_data['account'] = $account;
  }

  $password = getenv('cocoroboPassword');
  if (isset($password)) {
    $auth_data['password'] = $password;
  }

  $productId = getenv('cocoroboProductId');
  if (isset($productId)) {
    $auth_data['productId'] = $productId;
  }

  $userAgent = getenv('cocoroboProductId');
  if (isset($userAgent)) {
    $auth_data['userAgent'] = $userAgent;
  }

  //var_dump($auth_data);

  return $auth_data;
}

// ファイルに保存された音声発話データの読み出し
function cocoroboReadSpeechData($speech_data_file) {
  // 認証ファイルを開く
  $string = file_get_contents($speech_data_file);

  // 1行ごとに分解
  $lines = explode("\r\n", $string);
  return $lines;
}

// 音声発話データから1つをランダムに取得
function cocoroboSelectSpeech($speech_data) {
  $index = mt_rand(0, count($speech_data) - 1);
  return $speech_data[$index];
}


// 認証を実行
function cocoroboAuthenticate($auth_url, $auth_data) {
  // 認証に必要なデータの準備
  $post_array_data = array(
    'account' => $auth_data['account'],
    'password' => $auth_data['password']
  );

  // 認証データをJSON化
  $post_json_data = json_encode($post_array_data);

  // POSTのヘッダー
  $header = array(
    "Content-Type: application/json",
    "Content-Length: ".strlen($post_json_data),
    "User-Agent: ".$auth_data['userAgent']
  );

  // POSTのコンテクスト
  $context = array(
    'http' => array(
      'method' => 'POST',
      'header' => implode("\r\n", $header),
      'content' => $post_json_data
    )
  );

  // JSONデータのポストと応答の取得
  $response = file_get_contents($auth_url, false, stream_context_create($context));

  return $response;
}

// 音声発話を実行
function cocoroboSpeak($speech_url, $auth_data, $speech_text) {
  // 認証に必要なデータの準備
  $post_array_data = array(
    'account' => $auth_data['account'],
    'productId' => $auth_data['productId'],
    'message' => $speech_text
  );

  // 認証データをJSON化
  $post_json_data = json_encode($post_array_data);

  // POSTのヘッダー
  $header = array(
    "Content-Type: application/json",
    "Content-Length: ".strlen($post_json_data),
    "User-Agent: ".$auth_data['userAgent']
  );

  // POSTのコンテクスト
  $context = array(
    'http' => array(
      'method' => 'POST',
      'header' => implode("\r\n", $header),
      'content' => $post_json_data
    )
  );

  // JSONデータのポストと応答の取得
  $response = file_get_contents($speech_url, false, stream_context_create($context));

  return $response;
}