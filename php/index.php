<?php

// 認証のURL
$auth_url = "https://trial.cloudlabs.sharp.co.jp/cocorobo-api/ceAuth";

// 音声発話のURL
$speech_url = "https://trial.cloudlabs.sharp.co.jp/cocorobo-api/speech";



function postJSON($url, $json) {
  // POSTのヘッダー
  $header = array(
    "Content-Type: application/json",
    "Content-Length: ".strlen($json)
  );

  // POSTのコンテクスト
  $context = array(
    'http' => array(
      'method' => 'POST',
      'header' => implode("\r\n", $header),
      'content' => $json
    )
  );

  // JSONデータのポストと応答の取得
  $response = file_get_contents($url, false, stream_context_create($context));

  // デバッグ情報の出力
  var_dump($response);
}

// ファイルに保存された認証データの読み出し
function readAuthData(&$account, &$password, &$productId) {
  // 認証ファイルを開く
  $strings = file_get_contents("auth/auth.txt");

  // 1行ごとに分解
  $line = explode("\r\n", $strings);

  foreach($line as $item) {
    // 「:」で分解
    list($property, $value) = explode(":", $item);
    // accountとpasswordとproductIdのときだけ、値を設定する
    if ($property == "account") {
      $account = $value;
    } else if ($property == "password") {
      $password = $value;
    } else if ($property == "productId") {
      $productId = (int)$value;
    }
  }
}

// ファイルに保存された音声発話データの読み出し
function readSpeechData(&$message) {
  // 認証ファイルを開く
  $strings = file_get_contents("speech/speech.txt");

  // 1行ごとに分解
  $line = explode("\r\n", $strings);
  $index = mt_rand(0, count($line) - 1);
  $message = $line[$index];
}


// 認証データの読み出し
readAuthData($account, $password, $productId);

$auth_data = array(
  "account" => $account,
  "password" => $password
);

// 発話データの読み出し
$message = "教育事業大手のベネッセコーポレーション（岡山市）は社員が休日を分散取得できる取り組みを始めた。休祝日に出社する代わりに実験的に平日に休めるようにし、すでにゴールデンウイーク（ＧＷ";

readSpeechData($message);

$speech_data = array(
  "account" => $account,
  "productId" => $productId,
  "message" => $message
);

var_dump($auth_data);
var_dump($speech_data);

// 同一生成元ポリシーを解除するヘッダー
//header("Access-Control-Allow-Origin: *");

// COCOROBOのサーバーへの接続
postJSON($auth_url, json_encode($auth_data));
postJSON($speech_url, json_encode($speech_data));
